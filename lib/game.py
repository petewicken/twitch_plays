import win32api
import win32con
import time


class Game:

    keymap = {
        'up': 0x30,
        'up1': 0x30,
        'up 1': 0x30,
        'down': 0x31,
        'down1': 0x31,
        'down 1': 0x31,
        'left': 0x32,
        'left1': 0x32,
        'left 1': 0x32,
        'right': 0x33,
        'right1': 0x33,
        'right 1': 0x33,
        'a': 0x34,
        'a1': 0x34,
        'a2': 0x41,
        'a3': 0x53,
        'a4': 0x44,
        'a5': 0xBC,
        'a 1': 0x34,
        'a 2': 0x41,
        'a 3': 0x53,
        'a 4': 0x44,
        'a 5': 0xBC,
        'b': 0x35,
        'b1': 0x35,
        'b2': 0xBE,
        'b3': 0xBF,
        'b4': 0x6A,
        'b5': 0xDC,
        'b 1': 0x35,
        'b 2': 0xBE,
        'b 3': 0xBF,
        'b 4': 0x6A,
        'b 5': 0xDC,
        'start': 0x36,
        'select': 0x37,
        'upleft': 0x38,
        'upright': 0x39,
        'downleft': 0x4F,
        'downright': 0x50,
        'leftup': 0x54,
        'rightup': 0x59,
        'leftdown': 0x55,
        'rightdown': 0x49,
        'up2': 0x60,
        'up3': 0x61,
        'up4': 0x62,
        'up5': 0x63,
        'down2': 0x64,
        'down3': 0x65,
        'down4': 0x66,
        'down5': 0x67,
        'left2': 0x68,
        'left3': 0x69,
        'left4': 0x4B,
        'left5': 0x4C,
        'right2': 0x46,
        'right3': 0x47,
        'right4': 0x48,
        'right5': 0x4A,
        'front1': 0x5A,
        'front2': 0x58,
        'front3': 0x43,
        'front4': 0x56,
        'front5': 0x42,
        'swap1': 0x6B,
        'swap2': 0x6D,
        'swap3': 0x6F,
        'swap4': 0x6E,
        'swap5': 0xC0,
        'up6': 10,
        'up7': 11,
        'up8': 12,
        'up9': 13,
        'up10': 14,
        'down6': 15,
        'down7': 16,
        'down8': 17,
        'down9': 18,
        'down10': 19,
        'left6': 20,
        'left7': 21,
        'left8': 22,
        'left9': 23,
        'left10': 24,
        'right6': 25,
        'right7': 26,
        'right8': 27,
        'right9': 28,
        'right10': 29,
        'up 2': 0x60,
        'up 3': 0x61,
        'up 4': 0x62,
        'up 5': 0x63,
        'down 2': 0x64,
        'down 3': 0x65,
        'down 4': 0x66,
        'down 5': 0x67,
        'left 2': 0x68,
        'left 3': 0x69,
        'left 4': 0x4B,
        'left 5': 0x4C,
        'right 2': 0x46,
        'right 3': 0x47,
        'right 4': 0x48,
        'right 5': 0x4A,
        'front 1': 0x5A,
        'front 2': 0x58,
        'front 3': 0x43,
        'front 4': 0x56,
        'front 5': 0x42,
        'swap 1': 0x6B,
        'swap 2': 0x6D,
        'swap 3': 0x6F,
        'swap 4': 0x6E,
        'swap 5': 0xC0,
        'up 6': 10,
        'up 7': 11,
        'up 8': 12,
        'up 9': 13,
        'up 10': 14,
        'down 6': 15,
        'down 7': 16,
        'down 8': 17,
        'down 9': 18,
        'down 10': 19,
        'left 6': 20,
        'left 7': 21,
        'left 8': 22,
        'left 9': 23,
        'left 10': 24,
        'right 6': 25,
        'right 7': 26,
        'right 8': 27,
        'right 9': 28,
        'right 10': 29
    }

    def get_valid_buttons(self):
        return [button for button in self.keymap.keys()]

    def is_valid_button(self, button):
        return button in self.keymap.keys()

    def button_to_key(self, button):
        return self.keymap[button]

    def _do_push(self, key):
        win32api.keybd_event(0xA3, 0, 0, 0)
        win32api.keybd_event(key, 0, 0, 0)
        time.sleep(.15)
        win32api.keybd_event(key, 0, win32con.KEYEVENTF_KEYUP, 0)
        win32api.keybd_event(0xA3, 0, win32con.KEYEVENTF_KEYUP, 0)

    def push_button(self, button):
        if self.keymap[button] == 10:
            self._do_push(self.keymap['up'])
        elif self.keymap[button] == 11:
            self._do_push(self.keymap['down'])
        elif self.keymap[button] == 12:
            self._do_push(self.keymap['left'])
        elif self.keymap[button] == 13:
            self._do_push(self.keymap['right'])
        elif self.keymap[button] == 14:
            self._do_push(self.keymap['a'])
        elif self.keymap[button] == 15:
            self._do_push(self.keymap['b'])
        elif self.keymap[button] == 16:
            self._do_push(self.keymap['start'])
        elif self.keymap[button] == 17:
            self._do_push(self.keymap['select'])
        elif self.keymap[button] == 18:
            self._do_push(self.keymap['upleft'])
        elif self.keymap[button] == 19:
            self._do_push(self.keymap['upright'])
        elif self.keymap[button] == 20:
            self._do_push(self.keymap['up2'])
        elif self.keymap[button] == 21:
            self._do_push(self.keymap['up3'])
        elif self.keymap[button] == 22:
            self._do_push(self.keymap['up4'])
        elif self.keymap[button] == 23:
            self._do_push(self.keymap['up'])
        elif self.keymap[button] == 24:
            self._do_push(self.keymap['down2'])
        elif self.keymap[button] == 25:
            self._do_push(self.keymap['down3'])
        elif self.keymap[button] == 26:
            self._do_push(self.keymap['down4'])
        elif self.keymap[button] == 27:
            self._do_push(self.keymap['down5'])
        elif self.keymap[button] == 28:
            self._do_push(self.keymap['left2'])
        elif self.keymap[button] == 29:
            self._do_push(self.keymap['left3'])
        else:
            win32api.keybd_event(self.button_to_key(button), 0, 0, 0)
            time.sleep(.15)
            win32api.keybd_event(self.button_to_key(button), 0, win32con.KEYEVENTF_KEYUP, 0)
