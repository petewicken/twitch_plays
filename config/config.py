config = {

    'irc': {
        'server': 'irc.twitch.tv',
        'port': 6667,
        'channel': "retrobladen"
    },

    'account': {
        'username': 'username',
        'password': 'oauth:'  # http://twitchapps.com/tmi/
    },

    'throttled_buttons': {
        'start': 10
    },

    'misc': {
        'chat_height': 13,
        'cmd_prefix': '!'
    },

    # Make sure these exist!
    'whitelist': '/etc/twitch_plays/whitelist',
    'blacklist': '/etc/twitch_plays/blacklist',
    'goal': '/etc/twitch_plays/goal',

    'ops': [
        'retrobladen',
        'someone'
    ]

}
